# DangerFists
A Minecraft Minigame for use on Bukkit servers

### How do I get set up for development?
* Java 8
* Gradle
* Bukkit 1.12.*
* Configure artifact build (.jar) to include main compiled output and plugin.yml
* Developed using Intellij IDEA 

## Commands

| Command        | Description                                          |
|----------------|------------------------------------------------------|
| /dfCreate      | Create a DangerFist arena and return ID of new arena |
| /dfSetSpawn    | Create one spawn for a DangerFists arena             |
| /dfSetSpec     | Create spectator spawn for DangerFists arena         |
| /dfSetThrone   | Create throne room for DangerFists arena             |
| /dfArenaStatus | Status of the DangerFist arena                       |
| /dfSave        | Save DangerFist arena                                |
| /dfRemove      | Remove DangerFist arena                              |
| /dfStaging     | Begin a DangerFist round and allow players to join   |
| /dfStatus      | Status of DangerFist round                           |
| /dfRegister    | Register as a player for a DangerFist round          |
| /dfStart       | Start a DangerFist round                             |

## Classes
* Arena
* Match
* XPStore
* DangerFists

## Future Development
### Permissions
Currently permissions in DangerFists are naively implemented. 
Conversion to a standardized permissions plugin is required for wider acceptance
 and configuration.

### Contributing
Feel free to fork this repository and send pull requests!

## License
MIT License

Copyright (c) 2018 Sean Walsh

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.