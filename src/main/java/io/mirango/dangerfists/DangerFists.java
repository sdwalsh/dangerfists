package io.mirango.dangerfists;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class is the main plugin class for DangerFist
 * it contains the methods for handling command input from users
 * currently uses a naive solution to solve permissions (owner / admin v. everybody else)
 * DangerFists creates Arenas and Matches. Currently there is no saving of defined arenas and matches
 * on every restart the HashMaps are reset.
 *
 * @author Sean Walsh
 * @see Arena
 * @see Match
 * @see XPStore
 */
public class DangerFists extends JavaPlugin implements Listener
{
    private static final AtomicInteger arenaCounter = new AtomicInteger();
    private static final AtomicInteger matchCounter = new AtomicInteger();

    private HashMap<Integer, Arena> arenas = new HashMap<>();
    private HashMap<Integer, Match> matches = new HashMap<>();

    /**
     * {@inheritDoc}
     */
    @Override
    public void onEnable() {
        Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "DangerFists enabled. Version: " + getDescription().getVersion());
        Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "DangerFists enabled. Version: " + getDescription().getVersion());
        getServer().getPluginManager().registerEvents(this, this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDisable() {
        super.onDisable();
    }

    /**
     * Executes the given command, returning its success
     * We handle all our commands using a switch
     * multiple NumberFormatException are caught in try / catch blocks
     * Returns helpful error messages to user issuing command
     * Does not allow for console command senders
     *
     * @param sender Source of the command
     * @param command Command which was executed
     * @param label Alias of the command which was used
     * @param args Passed command arguments
     * @return true if a valid command, otherwise false
     */
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof ConsoleCommandSender) {
            sender.sendMessage("DangerFist commands are not run via console. Please login as a user!");
            return false;
        }
        Player player = (Player)sender;
        String cmd = command.getName().toLowerCase();
        switch (cmd){
            case "dfcreate": {
                sender.sendMessage("Successfully created arena ID: " + dfCreate(player));
                return true;
            }
            case "dfsetspawn": {
                int arena;
                int spawn;
                if(args.length < 2) {
                    sender.sendMessage("Not enough arguments");
                    return false;
                }
                try {
                    arena = Integer.parseInt(args[0]);
                    spawn = Integer.parseInt(args[1]);
                }
                catch(NumberFormatException e) {
                    sender.sendMessage("Sorry you didn't supply a valid number for one or more arguments");
                    return false;
                }
                if(dfSetPlayerSpawn(player, arena, spawn)) {
                    sender.sendMessage("Created spawn for player " + spawn + " at location " + player.getLocation());
                    return true;
                }
                return false;
            }
            case "dfsetspec": {
                int arena;
                if(args.length < 1) {
                    sender.sendMessage("Not enough arguments");
                    return false;
                }
                try {
                    arena = Integer.parseInt(args[0]);
                }
                catch(NumberFormatException e) {
                    sender.sendMessage("Sorry you didn't supply a valid number");
                    return false;
                }
                if(dfSetSpectatorSpawn(player, arena)) {
                    sender.sendMessage("Created spawn for spectators at " + player.getLocation());
                    return true;
                }
                return false;
            }
            case "dfsetthrone": {
                int arena;
                if(args.length < 1) {
                    sender.sendMessage("Not enough arguments");
                    return false;
                }
                try {
                    arena = Integer.parseInt(args[0]);
                } catch (NumberFormatException e) {
                    sender.sendMessage("Sorry you didn't supply a valid number");
                    return false;
                }
                if (dfSetThroneSpawn(player, arena)) {
                    sender.sendMessage("Created spawn for spectators at " + player.getLocation());
                    return true;
                }
                return false;
            }
            case "dfarenastatus":;
            case "dfsave":;
            case "dfremove":;
            case "dfstaging": {
                int arena;
                if(args.length < 1) {
                    sender.sendMessage("Not enough arguments");
                    return false;
                }
                try {
                    arena = Integer.parseInt(args[0]);
                } catch (NumberFormatException e) {
                    sender.sendMessage("Sorry you didn't supply a valid number");
                    return false;
                }
                int matchID = dfStaging(player, arena);
                if(matchID != -1) {
                    sender.sendMessage("Created match ID #: " + matchID);
                    return true;
                }
                return false;
            }
            case "dfstatus":;
            case "dfregister": {
                int match;
                if(args.length < 2) {
                    sender.sendMessage("Not enough arguments");
                    return false;
                }
                try {
                    match = Integer.parseInt(args[0]);
                } catch (NumberFormatException e) {
                    sender.sendMessage("Sorry you didn't supply a valid number");
                    return false;
                }
                String r = args[1].toLowerCase();
                Match.Role role;
                switch (r) {
                    case "throne": {
                        role = Match.Role.THRONE;
                        break;
                    }
                    case "player": {
                        role = Match.Role.PLAYER;
                        break;
                    }
                    case "spectator": {
                        role = Match.Role.SPECTATOR;
                        break;
                    }
                    default: {
                        sender.sendMessage("Role does not exist.");
                        return false;
                    }
                }
                if(dfRegister(player, match, role)) {
                    sender.sendMessage("Joined Match #: " + match + " as " + role.toString());
                    return true;
                }
                return false;
            }
            case "dfstart": {

            }
        }
        return false;
    }

    /**
     * Search for players to begin a match
     * takes an integer that represents an arena
     * returns an int that represents a match
     *
     * @param p a player - normally the player that calls the command
     * @param arena an identifier for the arena in which the match with be played
     * @return match ID from the matchCounter AtomicInteger
     */
    private int dfStaging(Player p, int arena) {
        if(!arenas.containsKey(arena)) {
            return -1;
        }
        Match match = new Match(p, arenas.get(arena));
        int matchNumber = matchCounter.getAndIncrement();
        matches.put(matchNumber, match);
        return matchNumber;
    }

    /**
     * Register as a player for a match
     * takes an integer that represents a match
     * returns true if successfully added as a user (will return false if already added or requesting taken role)
     *
     * @param p a player - normally the player that calls the command
     * @param match an identifier for the match
     * @param role a Match.Role enum (THRONE, PLAYER, SPECTATOR)
     * @return true if correctly added
     */
    private boolean dfRegister(Player p, int match, Match.Role role) {
        if(!matches.containsKey(match)) {
            return false;
        }
        Match m = matches.get(match);
        switch (role) {
            case THRONE: {
                return m.addThrone(p);
            }
            case PLAYER: {
                return m.addPlayer(p);
            }
            case SPECTATOR: {
                return m.addSpectator(p);
            }
        }
        return true;
    }
    /**
     * Determines if a player is authorized as a creator or admin for the
     * arena that they have requested
     *
     * @param p a player - normally the player that calls the command
     * @param arena an identifier for the arena
     * @return true if authorized
     */
    private boolean auth(Player p, int arena) {
        return arenas.containsKey(arena) && arenas.get(arena).authorized(p);
    }

    /**
     * Creates a DangerFists arena
     *
     * @param p a player - normally the player that calls the command
     * @return an identifier for the arena
     */
    private int dfCreate(Player p) {
        Arena arena = new Arena(p);
        int arenaNumber = arenaCounter.getAndIncrement();
        arenas.put(arenaNumber, arena);
        return arenaNumber;
    }

    /**
     * Sets the spawn point for throne (one player) in an arena
     *
     * @param p a player - normally the player that calls the command
     * @param arena an identifier for the arena
     * @return true if spawn has been set (false if unauthorized)
     */
    private boolean dfSetThroneSpawn(Player p, int arena) {
        Location location = p.getLocation();
        if(!auth(p, arena)) {
            return false;
        }
        Arena a = arenas.get(arena);
        a.setThroneSpawn(location);
        return true;
    }

    /**
     * Sets the spawn point for player in an arena
     *
     * @param p a player - normally the player that calls the command
     * @param arena an identifier for the arena
     * @param spawnNumber player spawn ID (up to 6 players)
     * @return true if player spawn was correctly set (false if unauthorized)
     */
    private boolean dfSetPlayerSpawn(Player p, int arena, int spawnNumber) {
        Location location = p.getLocation();
        if(!auth(p, arena)) {
            return false;
        }
        Arena a = arenas.get(arena);
        a.setPlayerSpawn(location, spawnNumber);
        return true;
    }

    /**
     * Sets the spawn point for spectators in an arena
     *
     * @param p a player -  normally the player that calls the command
     * @param arena an identifier for the arena
     * @return true if the spectator spawn was correctly set (false if unauthorized)
     */
    private boolean dfSetSpectatorSpawn(Player p, int arena) {
        Location location = p.getLocation();
        if(!auth(p, arena)) {
            return false;
        }
        Arena a = arenas.get(arena);
        a.setSpectatorSpawn(location);
        return true;
    }
}
