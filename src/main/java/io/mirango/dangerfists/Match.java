package io.mirango.dangerfists;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * A DangerFist match
 *
 * @author Sean Walsh
 * @see Arena
 * @see XPStore
 */
public class Match implements Listener {
    /**
     *  Playable roles for a match
     */
    public enum Role { THRONE, PLAYER, SPECTATOR };
    private Player owner;
    private Arena arena;
    private Player throne = null;
    private Set<Player> players = new HashSet<>();
    private Set<Player> spectators = new HashSet<>();
    private boolean inProgress = false;

    private XPStore xpStore = new XPStore();

    /**
     * Constructor for a Match
     * takes a player and an arena
     *
     * @param p a player that generally requested the match creation - will be listed as the owner
     * @param arena takes an arena for the match to be played
     */
    Match(Player p, Arena arena) {
        this.owner = p;
        this.arena = arena;
    }

    /**
     * Start a match
     * Stores all xp and levels for players before teleporting them to the starting location
     * will listen for deaths and declare a winner when only one remains
     */
    public void start() {
        // Create set of all players in a match
        HashSet<Player> allPlayers = getAllPlayers();
        // Prepare players for the match
        allPlayers.forEach(xpStore::store);
        Map<Player, Location> originalSpawns = storeSpawn(allPlayers);
        teleportPlayers();
    }

    /**
     * Store spawns for players that are starting a match
     *
     * @param allPlayers all the players that are starting a match
     * @return player -> location mapping of spawn points
     */
    private Map<Player, Location> storeSpawn(HashSet<Player> allPlayers) {
        Map<Player, Location> spawns = new HashMap<>();
        for(Player p : allPlayers) {
            spawns.put(p, p.getBedSpawnLocation());
        }
        return spawns;
    }

    /**
     * Restore the spawn locations for all players
     *
     * @param spawns a map of player -> location to restore
     */
    private void restoreSpawn(Map<Player, Location> spawns) {
        spawns.forEach((p, spawn) -> p.setBedSpawnLocation(spawn));
    }

    /**
     * Teleports players in the match (contained by players set, spectators set, and singular throne)
     * The players are teleported in no guaranteed order and if the number of spawns is already reached
     * players are teleported to the spectator spawn
     */
    private void teleportPlayers() {
        spectators.forEach(player -> player.teleport(arena.getSpectator()));
        throne.teleport(arena.getThrone());
        int playerNumber = 0;
        Map<Integer, Location> playerSpawns = arena.getPlayerSpawns();
        for(Player p : players) {
            if(playerSpawns.containsKey(playerNumber)) {
                p.teleport(playerSpawns.get(playerNumber));
            }
            else {
                p.teleport(arena.getSpectator());
            }
            playerNumber++;
        }
    }

    /**
     * Gets all players currently registered for the match
     *
     * @return set of all players
     */
    public HashSet<Player> getAllPlayers() {
        HashSet<Player> allPlayers = new HashSet<>();
        allPlayers.addAll(players);
        allPlayers.addAll(spectators);
        allPlayers.add(throne);
        return allPlayers;
    }

    /**
     * Gets the owner
     *
     * @return the player who created the match
     */
    public Player getOwner() {
        return owner;
    }

    /**
     * Adds a player to the Throne position
     *
     * @param throne player to be added to the throne position
     * @return true if successfully added to the role
     */
    public boolean addThrone(Player throne) {
        if(isPlayerInMatch(throne) || this.throne != null) {
            return false;
        }
        this.throne = throne;
        return true;
    }

    /**
     * Adds a player to the Players position
     *
     * @param player player to be added to the player position
     * @return true if successfully added to the role
     */
    public boolean addPlayer(Player player) {
        if(isPlayerInMatch(player) || players.size() >= arena.getPlayerSpawns().size()) {
            return false;
        }
        players.add(player);
        return true;
    }

    /**
     * Add a spectator to the game
     *
     * @param spectator a player to be added
     * @return true if player is added to the spectator pool
     */
    public boolean addSpectator(Player spectator) {
        if(isPlayerInMatch(spectator)) {
            return false;
        }
        this.spectators = spectators;
        return true;
    }

    /**
     * Removes a player from the match
     *
     * @param player a player - normally the player that is requesting deletion
     * @return true if removed or already not in the match
     */
    public boolean remove(Player player) {
        if(!isPlayerInMatch(player)) {
            return true;
        }
        if(throne.equals(player)) {
            throne = null;
            return true;
        }
        players.remove(player);
        spectators.remove(player);
        return true;
    }

    /**
     * Checks if a player is already in one of the three positions
     *
     * @param p a player - normally the player that is requesting addition
     * @return true if user is already in match
     */
    private boolean isPlayerInMatch(Player p) {
        return throne.equals(p) || players.contains(p) || spectators.contains(p);
    }
}
