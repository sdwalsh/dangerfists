package io.mirango.dangerfists;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * A DangerFist playing field
 *
 * @author Sean Walsh
 * @see Match
 * @see XPStore
 */
public class Arena {
    private Map<Integer, Location> playerSpawns = new HashMap<>();
    private Location throne = null;
    private Location spectator = null;
    private Player creator;
    private Set<Player> admins = new HashSet<>();

    /**
     * Constructor for a Arena
     * takes a player and gives full admin privileges
     *
     * @param creator a player - normally the player that calls the command
     */
    Arena(Player creator) {
        this.creator = creator;
    }

    /**
     * Checks if a player is authorized to access the arena
     *
     * @param p a player - normally the player that calls the command
     * @return ture if authorized
     */
    public boolean authorized(Player p) {
        return (creator.equals(p) || admins.contains(p));
    }

    /**
     * Adds an admin for the arena
     *
     * @param admin a player to be added as an authorized user
     */
    public void addAdmin(Player admin) {
        admins.add(admin);
    }

    /**
     * Removes an admin for the arena
     *
     * @param admin a player to be removed as an authorized user
     */
    public void removeAdmin(Player admin) {
        if (admins.contains(admin)) {
            admins.remove(admin);
        }
    }

    /**
     * Add player spawn at the given location
     *
     * @param spawn a bukkit location used to teleport the player
     * @param spawnNumber the player number (normally automatically supplied by an AtomicInteger)
     */
    public void setPlayerSpawn(Location spawn, Integer spawnNumber) {
        playerSpawns.put(spawnNumber, spawn);
    }

    /**
     * Add spectator spawn at the given location
     *
     * @param spectator a bukkit location used to teleport the spectators and on player death
     */
    public void setSpectatorSpawn(Location spectator) {
        this.spectator = spectator;
    }

    /**
     * Add throne spawn at the given location
     *
     * @param throne a bukkit location used to teleport the throne player
     */
    public void setThroneSpawn(Location throne) {
        this.throne = throne;
    }

    /**
     * Get the spectator spawn location
     *
     * @return spectator spawn location
     */
    public Location getSpectator() {
        return spectator;
    }

    /**
     * Get the player that created the arena
     *
     * @return player
     */
    public Player getCreator() {
        return creator;
    }

    /**
     * Get throne spawn location
     *
     * @return throne spawn location
     */
    public Location getThrone() {
        return throne;
    }

    /**
     * Get all admins for the arena
     *
     * @return set of all admins for the arena
     */
    public Set<Player> getAdmins() {
        return admins;
    }

    /**
     * Get all player spawn locations
     *
     * @return a map of the player spawn locations (6 possible keys)
     */
    public Map<Integer, Location> getPlayerSpawns() {
        return playerSpawns;
    }
}
