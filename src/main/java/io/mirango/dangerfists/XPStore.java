package io.mirango.dangerfists;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.HashMap;

/**
 * A simple level and experience store
 *
 * @author Sean Walsh
 * @see Match
 */
public class XPStore {
    private HashMap<Player, Float> xpTable = new HashMap<>();
    private HashMap<Player, Integer> levelTable = new HashMap<>();

    public Float getXP(Player player) {
        return xpTable.getOrDefault(player, 0F);
    }

    public Integer getLevel(Player player) {
        return levelTable.getOrDefault(player, 0);
    }

    public void setXP(Player player, Float xp) {
        xpTable.putIfAbsent(player, xp);
    }

    public void setLevel(Player player, Integer level) {
        levelTable.putIfAbsent(player, level);
    }

    /**
     * Store
     *
     * @param p
     */
    public void store(Player p) {
        Float experience = p.getExp();
        Integer level = p.getLevel();

        p.setExp(0F);
        p.setLevel(0);

        // If the player already has levels and xp stored add to it
        // otherwise place user into the tables with their current levels and xp
        if(xpTable.containsKey(p) && levelTable.containsKey(p)) {
            xpTable.put(p, experience + xpTable.get(p));
            levelTable.put(p, level + levelTable.get(p));
        }
        else {
            xpTable.put(p, experience);
            levelTable.put(p, level);
        }

        p.sendMessage(ChatColor.DARK_GRAY + "Successful! Stored " + level + " levels");
        p.sendMessage(ChatColor.DARK_GRAY + "Successful! Stored " + experience + " XP");
        p.sendMessage(ChatColor.DARK_GRAY + "Current Store: " + levelTable.get(p) + " levels " + xpTable.get(p) + " XP ");
    }

    public boolean restore(Player p) {
        Float experience = xpTable.get(p);
        Integer level = levelTable.get(p);
        if (experience != null && level != null) {
            xpTable.remove(p);
            levelTable.remove(p);

            p.setExp(experience);
            p.setLevel(level);

            p.sendMessage(ChatColor.DARK_GRAY + "Successful! Restored " + level + " levels");
            p.sendMessage(ChatColor.DARK_GRAY + "Successful! Restored " + experience + " XP");
            return true;
        }
        else {
            p.sendMessage(ChatColor.DARK_GRAY + "Oops.. can't seem to find you? Maybe you haven't stored your xp.");
            return false;
        }
    }

    public void restoreAll() {
        xpTable.forEach((k, v) -> {
           restore(k);
        });
    }
}

